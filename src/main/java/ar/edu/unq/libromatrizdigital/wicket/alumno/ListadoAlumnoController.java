package ar.edu.unq.libromatrizdigital.wicket.alumno;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.unq.libromatrizdigital.hibernate.OperationHome;
import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;

public class ListadoAlumnoController implements Serializable {
	private static final long serialVersionUID = 6824558483893635503L;
	
	private List<Alumno> alumnos = new ArrayList<>();
	private Alumno alumno;
	private String dniAlumno;
	private String apellido;
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDniAlumno() {
		return dniAlumno;
	}

	public void setDniAlumno(String dniAlumno) {
		this.dniAlumno = dniAlumno;
	}

	public ListadoAlumnoController() {
		this.alumnos = AlumnoStore.unico().getAlumnos();
	}

	public ListadoAlumnoController(Carrera carrera1) {
		this.alumnos = carrera1.getAlumnosInscriptos();
	}

	public ListadoAlumnoController(String dato) {			
		this.alumnos = OperationHome.getInstance().findByText(dato, Alumno.class); 
	}

	public List<Alumno> getAlumnos() {
		return this.alumnos.stream().sorted(Comparator.comparing(Alumno::getNombreCompleto))
				.collect(Collectors.toList());
	}

	public void setAlumno(Alumno _alumno) {
		this.alumno = _alumno;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public List<Alumno> getDatosDe(String dni) {// agregado 6 de mayo
		return this.getAlumnos().stream().filter(a -> a.getDni().equals(dni)).collect(Collectors.toList());
	}

}
