package ar.edu.unq.libromatrizdigital.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Carrera;

public class DesarchivarCarreraPage extends WebPage {
	private DesarchivarCarreraController controller;
	private static final long serialVersionUID = 1L;

	public DesarchivarCarreraPage(Carrera carrera) {
		this.controller = new DesarchivarCarreraController(carrera);// CarreraController(carrera);
		this.desarchivarCarrera();
	}

	private void desarchivarCarrera() {
		Form<DesarchivarCarreraController> carreraArchivada = new Form<DesarchivarCarreraController>(
				"desarchivarCarrera") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {

				DesarchivarCarreraPage.this.controller.activarCarreraArchivada();
				this.setResponsePage(new ListadoDeCarreras());
			}
		};
		carreraArchivada.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		carreraArchivada.add(new TextField<>("resolucion", new PropertyModel<>(this.controller, "resolucion")));
		carreraArchivada.add(new TextField<>("duracion", new PropertyModel<>(this.controller, "duracion")));

		this.add(carreraArchivada);

		carreraArchivada.add(new Link<String>("cancelar") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoDeCarreras());
			}

		});
	}

}
