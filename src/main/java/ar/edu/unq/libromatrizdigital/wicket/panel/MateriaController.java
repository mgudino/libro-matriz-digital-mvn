package ar.edu.unq.libromatrizdigital.wicket.panel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Cursada;
import ar.edu.unq.libromatrizdigital.model.EstadoCursada;
import ar.edu.unq.libromatrizdigital.model.Materia;
import ar.edu.unq.libromatrizdigital.wicket.HomePage;
import ar.edu.unq.libromatrizdigital.wicket.matriculacion.NoPudoMatricularsePage;
import ar.edu.unq.libromatrizdigital.wicket.matriculacion.PudoMatricularsePage;

public class MateriaController implements Serializable{
	private Carrera carrera;
	private Materia materia;
	private Alumno alumno;

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public List<Materia> getMateriasNoCursadas() {
		List<Materia> materiasList = new ArrayList<>(this.getAlumno().getMateriasPorCursarEnCarrera(this.getCarrera()));
		return materiasList;
	}

	public void matricular(HomePage page) {
		if (getAlumno().puedeMatricularseA(getMateria())) {
			Cursada cursada = new Cursada(EstadoCursada.CURSANDO);
			cursada.setMateria(getMateria());
			
			getAlumno().agregarCursada(cursada);
			page.setResponsePage(new PudoMatricularsePage(getAlumno(), getMateria()));
		} else {
			page.setResponsePage(new NoPudoMatricularsePage(getAlumno(), getMateria()));
		}
	}
}
