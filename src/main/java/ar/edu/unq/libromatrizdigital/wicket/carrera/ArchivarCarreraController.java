package ar.edu.unq.libromatrizdigital.wicket.carrera;

import java.io.Serializable;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.OfertaAcademicaStore;

public class ArchivarCarreraController implements Serializable {

	private static final long serialVersionUID = 1L;
	private Carrera carrera;
	private String nombre;
	private String resolucion;
	private double duracion;

	public ArchivarCarreraController(Carrera carrera) {
		this.carrera = carrera;
		this.nombre = carrera.getNombre();
		this.resolucion = carrera.getResolucion();
		this.duracion = carrera.getDuracion();
	}

	public void archivarCarrera() {
		OfertaAcademicaStore.instituto().archivarCarrera(carrera);
	}

}
