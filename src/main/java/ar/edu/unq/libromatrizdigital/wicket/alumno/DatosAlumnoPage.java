package ar.edu.unq.libromatrizdigital.wicket.alumno;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.libromatrizdigital.model.Alumno;

public class DatosAlumnoPage extends WebPage {
	private static final long serialVersionUID = -1061824403496088979L;

	private DatosAlumnoController controller;
	protected Alumno alumnoSeleccionado;

	public DatosAlumnoPage(Alumno alumnoSeleccionado) {
		super();
		this.controller = new DatosAlumnoController(alumnoSeleccionado);
		this.datosAlumnoForm();
	}

	private void datosAlumnoForm() {
		Form<DatosAlumnoController> datosAlumnoForm = new Form<DatosAlumnoController>("datosAlumnoForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				controller.modificarAlumno();
				this.setResponsePage(new ListadoAlumnoPage());
			}
		};

		datosAlumnoForm.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		datosAlumnoForm.add(new TextField<>("apellido", new PropertyModel<>(this.controller, "apellido")));
		datosAlumnoForm.add(new TextField<>("dni", new PropertyModel<>(this.controller, "dni")));
		datosAlumnoForm.add(new TextField<>("telefono", new PropertyModel<>(this.controller, "telefono")));
		datosAlumnoForm.add(new TextField<>("calle", new PropertyModel<>(this.controller, "calle")));
		datosAlumnoForm.add(new TextField<>("numero", new PropertyModel<>(this.controller, "numero")));
		datosAlumnoForm.add(new TextField<>("localidad", new PropertyModel<>(this.controller, "localidad")));
		datosAlumnoForm.add(new TextField<>("email", new PropertyModel<>(this.controller, "email")));

		datosAlumnoForm.add(new DropDownChoice<>("carrera", new PropertyModel<>(this.controller, "carrera"),
				new PropertyModel<>(this.controller, "carreras"), new ChoiceRenderer<>("nombre")
		));
		
		datosAlumnoForm.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = -94548880135660927L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoAlumnoPage());
			}
		});
		
		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = -5103954084634722612L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoAlumnoPage());
			}
		});

		this.add(datosAlumnoForm);
	}

}
	
