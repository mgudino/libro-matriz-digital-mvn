package ar.edu.unq.libromatrizdigital.wicket.alumno;

import java.io.Serializable;

import ar.edu.unq.libromatrizdigital.model.Alumno;
import ar.edu.unq.libromatrizdigital.model.Direccion;

public class AgregarAlumnoController implements Serializable {

	private static final long serialVersionUID = 2508720547683154265L;

	private Alumno alumno;
	private String nombre;
	private String apellido;
	private String dni;
	private String telefono;
	private String email;
	private String calle;
	private String numero;
	private String localidad;
	private Direccion direccion;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Alumno getAlumno() {
		return this.alumno;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getApellido() {
		return this.apellido;
	}

	public String getDni() {
		return this.dni;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void agregarAlumno() {
		if (this.datosAlumnoBienCargados()) {
			Direccion direccion = new Direccion(this.getCalle(), this.getNumero(), this.getLocalidad());
			Alumno alumno = new Alumno(this.getNombre(), this.getApellido(), this.getDni(), this.getTelefono(),
					direccion, this.getEmail());

			AlumnoStore.unico().agregarAlumno(alumno, direccion);

		}
	}

	private boolean datosAlumnoBienCargados() {
		return this.getNombre() != null && this.getApellido() != null && this.getDni() != null
				&& this.getTelefono() != null && this.getCalle() != null && this.getNumero() != null
				&& this.getLocalidad() != null && this.getEmail() != null;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

}
