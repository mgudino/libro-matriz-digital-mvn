package ar.edu.unq.libromatrizdigital.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Docente extends Persistible {
	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String apellido;
	
	@OneToOne
	private Direccion direccion;
	
	private String telefono;
	
	public Docente() {
		super();
	}
	
	public Docente(String nombre, String apellido, Direccion direccion, String telefono) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
}