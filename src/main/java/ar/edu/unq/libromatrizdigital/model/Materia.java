package ar.edu.unq.libromatrizdigital.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

@Entity
public class Materia extends Persistible{
	
	@ManyToOne
	private Carrera carrera;
	private String nombre;
	
	@Type(type="yes_no")
	private boolean esPromocionable;
	
	@Transient
	private Set<Materia> correlativas = new HashSet<>();

	public void baja(Materia materia) { // para verificar que esta la materia q voy a eliminar
		if (carrera.getMaterias().contains(materia)) {
			carrera.getMaterias().remove(materia);
		}
	}

	public Materia() {
	}

	public Materia(String nombre, boolean esPromocionable) {
		this.nombre = nombre;
		this.esPromocionable = esPromocionable;
	}

	public Materia(Carrera carrera, String nombre, boolean esPromocionable) {
		super();
		this.carrera = carrera;
		this.nombre = nombre;
		this.esPromocionable = esPromocionable;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean esPromocionable() {
		return esPromocionable;
	}

	public void setEsPromocionable(boolean esPromocionable) {
		this.esPromocionable = esPromocionable;
	}

	public String getPromocionable() {
		if (this.esPromocionable == true) {
			return "Si";
		}
		return "No";
	}

	public Set<Materia> getCorrelativas() {
		return correlativas;
	}

	public void setCorrelativas(Set<Materia> correlativas) {
		this.correlativas = correlativas;
	}

	public void borrarCorrelativa(Materia materia) {
		this.correlativas.remove(materia);
	}

	public void agregarCorrelativa(Materia materia) {
		this.correlativas.add(materia);
	}

	public Carrera getCarrera() {
		return this.carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
}