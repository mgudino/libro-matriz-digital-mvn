package ar.edu.unq.libromatrizdigital.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import ar.edu.unq.libromatrizdigital.model.Carrera;
import ar.edu.unq.libromatrizdigital.model.Materia;

public class MateriaTest {

	@Test
	public void puedeTenerUnNombre() {
		Carrera tip = new Carrera();
		Materia mate = new Materia("mate", false);
		Materia didactica = new Materia("Didáctica I", false);
		tip.agregarMateria(didactica);

		assertEquals(Arrays.asList(didactica), tip.getMaterias());
		assertTrue(tip.puedoAgregarMateria(mate));
	}

}

